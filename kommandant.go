package kommandant

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/chzyer/readline"
	"io"
	"os"
	"reflect"
	"sort"
	"strings"
)

type KmdtClient interface {
	SetCore(k *Kmdt)
}

var typeOfBool = reflect.TypeOf(false)
var typeOfString = reflect.TypeOf("")
var typeOfStringList = reflect.TypeOf([]string{})

type Kmdt struct {
	Sub          KmdtClient // The struct which will have all the handlers
	CommandQueue []string
	Intro        string
	LastCmd      string
	Ruler        string
	DocLeader    string
	DocHeader    string
	UnDocHeader  string
	MiscHeader   string
	NoHelp       string
	// Private fields
	stdin        io.ReadCloser
	buffer_stdin *bufio.Reader
	stdout       io.Writer
	prompt       string // Handled through SetPrompt
	rlconfig     readline.Config
	// Characters used in command names
	IdentChars string
	// Special hooks
	// =============
	// Hook run in place of OneCmd if present
	OneCommand func(lineIn string) (stopOut bool)
	// Hook executed once when the cmdloop() method is called.
	PreLoop func()
	// Hook executed once when the cmdloop() method is about to return.
	PostLoop func()
	// Hook executed just before the command line is interpreted, but after
	// the input prompt is generated and issued.
	PreCommand func(lineIn string) (lineOut string)
	// Hook executed just after a command dispatch is finished.
	PostCommand func(stopIn bool, lineIn string) (stopOut bool)
	// Called when an empty line is entered in response to the prompt.
	// If this is not set, the last nonempty command entered will be repeated.
	EmptyLine func() (stopOut bool)
	// Called on an input line when the command prefix is not recognized.
	// If this is not set, an error message is printed and it and returns.
	DefaultCommand func(lineIn string) (stopOut bool)
	// Function called to complete an input line when no command-specific
	// complete function is available. By default, it returns an empty list.
	CompleteDefault func(line string) (options []string)
	// Bound methods
	handlers map[string]func(lineIn string) (stopOut bool)
	// Bound help methods
	helpers map[string]func()
	// Bound completers
	completers    map[string]func(string) []string
	rootCompleter *readline.PrefixCompleter
	// Readline data
	linecore *readline.Instance
}

// Instantiate an interpreter framework.

func NewKommandant(handlers KmdtClient) *Kmdt {
	kmdt := &Kmdt{}
	// Weld the objects
	handlers.SetCore(kmdt)
	kmdt.Sub = handlers
	// Validate and load provided handlers
	kmdt.bind(handlers)
	// Setup basic IO
	kmdt.SetStdin(os.Stdin)
	kmdt.SetStdout(os.Stdout)
	kmdt.init_Kommandant()
	kmdt.SetPrompt("(Kmdt) ")
	return kmdt
}

func (k *Kmdt) EnableReadline(enable bool) (err error) {
	if enable {
		cfg := k.buildReadlineConfig()
		k.linecore, err = readline.NewEx(&cfg)
		return err
	}
	if k.linecore != nil {
		k.linecore.Close()
	}
	k.linecore = nil
	return nil
}

// Combines any custom settings in rlconfig with required settings
func (k *Kmdt) buildReadlineConfig() (newConfig readline.Config) {
	newConfig = k.rlconfig
	newConfig.Prompt = k.prompt
	k.buildCompletion()
	newConfig.AutoComplete = k.rootCompleter
	newConfig.Stdin = k.stdin
	newConfig.Stdout = k.stdout
	return newConfig
}

func (k *Kmdt) ReadlineEnabled() (enabled bool) {
	return k.linecore != nil
}

func (k *Kmdt) SetStdin(stdin io.ReadCloser) {
	k.stdin = stdin
	k.buffer_stdin = bufio.NewReader(k.stdin)
}

func (k *Kmdt) GetStdin() (stdin io.ReadCloser) {
	return k.stdin
}

func (k *Kmdt) SetStdout(stdout io.Writer) {
	k.stdout = stdout
}

func (k *Kmdt) GetStdout() (stdout io.Writer) {
	return k.stdout
}

func (k *Kmdt) SetPrompt(prompt string) {
	k.prompt = prompt
	if k.linecore != nil {
		k.linecore.SetPrompt(prompt)
	}
}

func (k *Kmdt) GetPrompt() (prompt string) {
	return k.prompt
}

func (k *Kmdt) init_Kommandant() {
	k.CommandQueue = make([]string, 0)
	k.IdentChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQUSTUVWXYZ0123456789_"
	k.Ruler = "="
	k.LastCmd = ""
	k.Intro = ""
	k.DocLeader = ""
	k.DocHeader = "Documented commands (type help <topic>):"
	k.UnDocHeader = "Undocumented commands:"
	k.MiscHeader = "Miscellaneous help topics:"
	k.NoHelp = "*** No help on %s"
}

func (k *Kmdt) bind(hand KmdtClient) {
	k.handlers = make(map[string]func(string) bool, 0)
	k.helpers = make(map[string]func(), 0)
	k.completers = make(map[string]func(string) []string, 0)
	// Typecheck and store valid handlers from the KmdtClient
	v := reflect.ValueOf(hand)
	t := reflect.TypeOf(hand)
	for i := 0; i < t.NumMethod(); i++ {
		tm := t.Method(i)
		vm := v.Method(i)
		// Search for handlers
		// TODO: completion handler checks will be needed

		// Note that, unlike tm, vm has a Curry'ed receiver.
		// Specifically the receiver is v.
		if isHandler(tm) {
			commandName := tm.Name
			if strings.HasPrefix(commandName, "Do") {
				commandName = strings.ToLower(commandName[len("Do"):])
			} else if commandName == "Default" {
				commandName = strings.ToLower(commandName)
			} else {
				// invalid
				continue
			}
			// Store the handler
			checkedMethod := vm.Interface().(func(string) bool)
			k.handlers[commandName] = checkedMethod
		} else if isCompleter(tm) {
			completerName := strings.ToLower(tm.Name[len("Complete"):])
			checkedMethod := vm.Interface().(func(string) []string)
			k.completers[completerName] = checkedMethod
		} else if isHelper(tm) {
			checkedMethod := vm.Interface().(func())
			commandName := strings.ToLower(tm.Name[len("Help"):])
			k.helpers[commandName] = checkedMethod
		} else if isPreLoop(tm) {
			checkedMethod := vm.Interface().(func())
			k.PreLoop = checkedMethod
		} else if isPostLoop(tm) {
			checkedMethod := vm.Interface().(func())
			k.PostLoop = checkedMethod
		} else if isPreCommand(tm) {
			checkedMethod := vm.Interface().(func(string) string)
			k.PreCommand = checkedMethod
		} else if isPostCommand(tm) {
			checkedMethod := vm.Interface().(func(bool, string) bool)
			k.PostCommand = checkedMethod
		} else if isDefaultCommand(tm) {
			checkedMethod := vm.Interface().(func(string) bool)
			k.DefaultCommand = checkedMethod
		} else if isEmptyLine(tm) {
			checkedMethod := vm.Interface().(func() bool)
			k.EmptyLine = checkedMethod
		} else if isDefaultComplete(tm) {
			checkedMethod := vm.Interface().(func(string) []string)
			k.CompleteDefault = checkedMethod
		} else {
			// Not a valid handler
			continue
		}
	}
}

func isHandler(method reflect.Method) bool {
	return (method.Type.NumIn() == 2) &&
		(method.Type.In(1) == typeOfString) &&
		(method.Type.NumOut() == 1) &&
		(method.Type.Out(0) == typeOfBool) &&
		(strings.HasPrefix(method.Name, "Do"))
}

func isCompleter(method reflect.Method) bool {
	return (method.Type.NumIn() == 2) &&
		(method.Type.In(1) == typeOfString) &&
		(method.Type.NumOut() == 1) &&
		(method.Type.Out(0) == typeOfStringList) &&
		(strings.HasPrefix(method.Name, "Complete"))
}

func isHelper(method reflect.Method) bool {
	return (method.Type.NumIn() == 1) &&
		(method.Type.NumOut() == 0) &&
		strings.HasPrefix(method.Name, "Help")
}

func isPreLoop(method reflect.Method) bool {
	return (method.Type.NumIn() == 1) &&
		(method.Type.NumOut() == 0) &&
		(method.Name == "PreLoop")
}

func isPostLoop(method reflect.Method) bool {
	return (method.Type.NumIn() == 1) &&
		(method.Type.NumOut() == 0) &&
		(method.Name == "PostLoop")
}

func isPreCommand(method reflect.Method) bool {
	return (method.Type.NumIn() == 2) &&
		(method.Type.In(1) == typeOfString) &&
		(method.Type.NumOut() == 1) &&
		(method.Type.Out(0) == typeOfString) &&
		(method.Name == "PreCommand")
}

func isPostCommand(method reflect.Method) bool {
	return (method.Type.NumIn() == 3) &&
		(method.Type.In(1) == typeOfBool) &&
		(method.Type.In(2) == typeOfString) &&
		(method.Type.NumOut() == 1) &&
		(method.Type.Out(0) == typeOfBool) &&
		(method.Name == "PostCommand")
}

func isDefaultCommand(method reflect.Method) bool {
	return (method.Type.NumIn() == 2) &&
		(method.Type.In(1) == typeOfString) &&
		(method.Type.NumOut() == 1) &&
		(method.Type.Out(0) == typeOfBool) &&
		(method.Name == "DefaultCommand")
}

func isEmptyLine(method reflect.Method) bool {
	return (method.Type.NumIn() == 1) &&
		(method.Type.NumOut() == 1) &&
		(method.Type.Out(0) == typeOfBool) &&
		(method.Name == "EmptyLine")
}

func isDefaultComplete(method reflect.Method) bool {
	return (method.Type.NumIn() == 2) &&
		(method.Type.NumOut() == 1) &&
		(method.Type.In(1) == typeOfString) &&
		(method.Type.Out(0) == typeOfStringList) &&
		(method.Name == "DefaultComplete")
}

// Repeatedly issue a prompt, accept input, parse an initial prefix
// off the received input, and dispatch to action methods, passing them
// the remainder of the line as argument.
func (k *Kmdt) CmdLoop(intro string) {
	if k.PreLoop != nil {
		k.PreLoop()
	}
	// if self.use_rawinput and self.completekey
	// readline completer stuff
	// NOTE: this is setting up our completer
	// Currently ignoring fancy features
	if intro != "" {
		k.Intro = intro
	}
	if k.Intro != "" {
		if k.linecore == nil {
			k.WriteString(k.Intro + "\n")
		} else {
			println(k.Intro)
		}
	}
	// if k.linecore != nil start
	stop := false
	line := ""
	for !stop {
		if len(k.CommandQueue) > 0 {
			//line = k.CommandQueue.pop(0)
			line, k.CommandQueue = k.CommandQueue[0], k.CommandQueue[1:]
		} else {
			if k.linecore != nil {
				var err error
				line, err = k.linecore.Readline()
				if err == readline.ErrInterrupt {
					if len(line) == 0 {
						break // We are done here; bail.
					} else {
						// What?
					}
				} else if err == io.EOF {
					break // Nothing left to do; bail.
				}
				line = strings.TrimSpace(line)
			} else {
				k.WriteString(k.prompt)
				var err error
				line, err = k.buffer_stdin.ReadString('\n')
				if err == io.EOF && line == "" {
					line = "EOF"
				} else {
					line = strings.Trim(line, "\r\n")
				}
			}
		}
		if k.PreCommand != nil {
			line = k.PreCommand(line)
		}
		stop = k.OneCmd(line)
		if k.PostCommand != nil {
			stop = k.PostCommand(stop, line)
		}
	}
	if k.PostLoop != nil {
		k.PostLoop()
	}
	// Take down our completer
}

// Parse the line into a command name and a string containing
// the arguments.  Returns a tuple containing (command, args, line).
// 'command' and 'args' may be None if the line couldn't be parsed.
func (k *Kmdt) ParseLine(lineIn string) (cmd string, arg string, lineOut string) {
	lineOut = strings.TrimSpace(lineIn)
	if lineOut == "" {
		return "", "", lineOut
	} else if lineOut[0] == '?' {
		lineOut = "help " + lineOut[1:]
	} else if lineOut[0] == '!' {
		// look for "shell" in handler map
		if _, ok := k.handlers["shell"]; ok == true {
			lineOut = "shell " + lineOut[1:]
		} else {
			return "", "", lineOut
		}
	}
	i, n := 0, len(lineOut)
	// while i < n and line[i] in self.identchars: i = i+1
	for i < n {
		found := false
		for charI := 0; charI < len(k.IdentChars); charI++ {
			if lineOut[i] == k.IdentChars[charI] {
				found = true
				break
			}
		}
		if found {
			i++
		} else {
			break
		}
	}
	cmd = lineOut[:i]
	arg = strings.TrimSpace(lineOut[i:])
	return cmd, arg, lineOut
}

func (k *Kmdt) OneCmd(lineIn string) (stopOut bool) {
	if k.OneCommand != nil {
		return k.OneCommand(lineIn)
	} else {
		return k.OneCmd_core(lineIn)
	}
}

// Interpret the argument as though it had been typed in response to the
// prompt.
func (k *Kmdt) OneCmd_core(lineIn string) (stopOut bool) {
	//fmt.Println("OneCmd:", lineIn)
	cmd, arg, line := k.ParseLine(lineIn)
	// Handle empty line
	if line == "" {
		if k.EmptyLine != nil {
			return k.EmptyLine()
		} else {
			return false
		}
	}
	// Handle no command
	// The only way we get cmd == "" and arg == "" but line != "" is if one
	// of the special commands triggered and failed.
	if (cmd == "") && (arg == "") {
		return k.runDefault(line)
	}
	// Set last command
	k.LastCmd = line
	// Check for EOF
	if line == "EOF" {
		k.LastCmd = ""
	}
	// Check empty command
	if cmd == "" {
		return k.runDefault(line)
	} else {
		// Look for handler
		hand, ok := k.handlers[strings.ToLower(cmd)]
		// Check for special handlers
		// Do_help, completer
		if ok {
			return hand(arg)
		} else if cmd == "help" {
			if hand, ok = k.handlers["help"]; ok == true {
				hand(arg)
			} else {
				// Do default help
				k.DoHelp(arg)
			}
			return false
		} else {
			return k.runDefault(line)
		}
	}
}

func (k *Kmdt) runDefault(lineIn string) (stopOut bool) {
	if k.DefaultCommand != nil {
		return k.DefaultCommand(lineIn)
	} else {
		return k.defaultCmd(lineIn)
	}
}

func (k *Kmdt) defaultCmd(lineIn string) (stopOut bool) {
	k.WriteString("*** Unknown syntax: " + lineIn + "\n")
	return false
}

func (k *Kmdt) DoHelp(argIn string) (stopOut bool) {
	if argIn != "" {
		// Look for a Help* function
		helper, ok := k.helpers[argIn]
		if ok == true {
			helper()
			return false
		} else {
			// Nothing found, default
			k.WriteString(fmt.Sprintf(k.NoHelp+"\n", argIn))
		}
	} else {
		cmds_doc := []string{}
		cmds_undoc := []string{}
		misc_help_map := make(map[string]bool, 0)
		for c := range k.helpers {
			misc_help_map[c] = true
		}
		// Sort helpers into cmd+help, cmd-alone, help-alone
		for cmd, _ := range k.handlers {
			if _, ok := k.helpers[cmd]; ok {
				// Documented command
				cmds_doc = append(cmds_doc, cmd)
				delete(misc_help_map, cmd)
			} else {
				// Undocumented command
				if cmd == "eof" {
					continue
				}
				cmds_undoc = append(cmds_undoc, cmd)
			}
		}
		// Convert misc_help to a list
		misc_help := make([]string, len(misc_help_map))
		i := 0
		for c := range misc_help_map {
			misc_help[i] = c
			i++
		}
		// Sort to dodge map key ordering randomness
		sort.Strings(cmds_doc)
		sort.Strings(misc_help)
		sort.Strings(cmds_undoc)
		// Format and output
		k.WriteString(k.DocLeader + "\n")
		k.printTopics(k.DocHeader, cmds_doc, 80)
		k.printTopics(k.MiscHeader, misc_help, 80)
		k.printTopics(k.UnDocHeader, cmds_undoc, 80)
	}
	return false
}

func (k *Kmdt) Write(data []byte) {
	if k.linecore != nil { // We are using readline here
		k.linecore.Write(data)
	} else if k.stdout != nil { // Using normal output
		k.stdout.Write(data)
	}
}

func (k *Kmdt) WriteString(data string) {
	k.Write([]byte(data))
}

func (k *Kmdt) printTopics(header string, cmds []string, maxcol int) {
	if len(cmds) > 0 {
		k.WriteString(header + "\n")
		if k.Ruler != "" {
			ruler := strings.Repeat(k.Ruler, len(header))
			k.WriteString(ruler + "\n")
		}
		col := columnize(cmds, maxcol-1)
		k.WriteString(col + "\n")
	}
}

// Display a list of strings as a compact set of columns.
// ------------------------------------------------------
// Each column is only as wide as necessary.
// Columns are separated by two spaces (one was not legible enough).
func columnize(strs []string, displayWidth int) (formatted string) {
	if (strs == nil) || (len(strs) < 1) {
		return "<empty>\n"
	}
	nrows, colwidths := calculateColumns(strs, displayWidth)
	formatted = formatColumns(strs, nrows, colwidths)
	return formatted
}

// Calculate position data for columnize
func calculateColumns(strs []string, displayWidth int) (nrows int, colwidths []int) {
	var colwidth, totwidth, xlen int
	size := len(strs)
	foundMatch := false // Go does not have for...else construct
	for nrows = 1; nrows < size; nrows++ {
		ncols := (size + nrows - 1) / nrows
		colwidths = make([]int, 0)
		totwidth = -2
		for col := 0; col < ncols; col++ {
			colwidth = 0
			for row := 0; row < nrows; row++ {
				i := row + (nrows * col)
				if i >= size {
					break
				}
				x := strs[i]
				// colwidth = max(colwidth, len(x))
				xlen = len(x)
				if colwidth > xlen {
					colwidth = colwidth
				} else {
					colwidth = xlen
				}
			}
			colwidths = append(colwidths, colwidth)
			totwidth += colwidth + 2
			if totwidth > displayWidth {
				break
			}
		}
		if totwidth <= displayWidth {
			foundMatch = true
			break
		}
	}
	if foundMatch == false {
		nrows = size
		colwidths = []int{0}
	}
	return nrows, colwidths
}

// Format according to precalculated position data
func formatColumns(strs []string, nrows int, colwidths []int) (formatted string) {
	formatted = ""
	ncols := len(colwidths)
	size := len(strs)
	for row := 0; row < nrows; row++ {
		texts := make([]string, 0)
		for col := 0; col < ncols; col++ {
			i := row + (nrows * col)
			if i >= size {
				texts = append(texts, "")
			} else {
				texts = append(texts, strs[i])
			}
		}
		// while texts and not texts[-1]:
		//     del texts[-1]
		textsLimit := len(texts) - 1
		for (textsLimit > 0) && (texts[textsLimit] == "") {
			textsLimit--
		}
		for col := 0; col < textsLimit; col++ {
			// texts[col] = texts[col].ljust(colwidths[col])
			spaceCount := colwidths[col] - len(texts[col])
			spaces := strings.Repeat(" ", spaceCount)
			texts[col] = texts[col] + spaces
		}
		// self.stdout.write("%s\n"%str("  ".join(texts)))
		formatted = formatted + strings.Join(texts[:textsLimit+1], "  ") + "\n"
	}
	return formatted
}

// The normal readline completion handling requires a seperate dynamic
// callback for each layer of the completion. This allows the same callback
// to handle multiple arguments on a single command.
type RecursiveCompleter struct {
	Completer      readline.PrefixCompleter
	MaxRecurse     int
	CurrentRecurse int
}

func (r *RecursiveCompleter) Print(prefix string, level int, buf *bytes.Buffer) {
	r.Completer.Print(prefix, level, buf)
}

func (r *RecursiveCompleter) Do(line []rune, pos int) (newLine [][]rune, length int) {
	return r.Completer.Do(line, pos)
}

func (r *RecursiveCompleter) GetName() []rune {
	return r.Completer.GetName()
}

func (r *RecursiveCompleter) GetChildren() []readline.PrefixCompleterInterface {
	children := r.Completer.GetChildren()
	if children == nil || len(children) < 1 {
		// No defined children, recurse
		if r.CurrentRecurse < r.MaxRecurse {
			r.CurrentRecurse++
			return []readline.PrefixCompleterInterface{r}
		} else {
			r.CurrentRecurse = 0
			return children
		}
	} else {
		return children
	}
}

func (r *RecursiveCompleter) SetChildren(children []readline.PrefixCompleterInterface) {
	r.Completer.SetChildren(children)
}

func (r *RecursiveCompleter) IsDynamic() bool {
	return r.Completer.IsDynamic()
}

func (r *RecursiveCompleter) GetDynamicNames(line []rune) [][]rune {
	return r.Completer.GetDynamicNames(line)
}

func PcItemDynamicRecursive(callback readline.DynamicCompleteFunc) readline.PrefixCompleterInterface {
	return &RecursiveCompleter{
		Completer: readline.PrefixCompleter{
			Callback: callback,
			Dynamic:  true,
		},
		MaxRecurse:     31,
		CurrentRecurse: 0,
	}
}

func (k *Kmdt) buildCompletion() {
	// Construct completion tree from handlers
	var handList []readline.PrefixCompleterInterface
	for handlerName, _ := range k.handlers {
		if completer, ok := k.completers[handlerName]; ok {
			// Custom completer available
			comp := PcItemDynamicRecursive(completer)
			handList = append(handList, readline.PcItem(handlerName, comp))
		} else {
			if k.CompleteDefault == nil {
				// No custom completer, no default completer
				handList = append(handList, readline.PcItem(handlerName))
			} else {
				// No custom completer, but have a default completer
				comp := readline.PcItemDynamic(k.CompleteDefault)
				handList = append(handList, readline.PcItem(handlerName, comp))
			}
		}
	}
	var helpList []readline.PrefixCompleterInterface
	for helperName, _ := range k.helpers {
		helpList = append(helpList, readline.PcItem(helperName))
	}
	handList = append(handList, readline.PcItem("help", helpList...))
	k.rootCompleter = readline.NewPrefixCompleter(handList...)
}
