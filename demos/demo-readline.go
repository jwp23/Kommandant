package main

import (
	"fmt"
	"gitlab.com/ianbruene/Kommandant"
	"strings"
)

var prompts = map[bool]string{false: "basic> ", true: "readline> "}

type sample struct {
	core *kommandant.Kmdt
}

func (s *sample) SetCore(k *kommandant.Kmdt) {
	s.core = k
}

func (s *sample) DoEOF(lineIn string) (stopOut bool) {
	return false
}

func (s *sample) DoQuit(lineIn string) (stopOut bool) {
	return true
}

func (s *sample) DoPrompt(lineIn string) (stopOut bool) {
	s.core.SetPrompt(lineIn + " ")
	return false
}

func (s *sample) DoFoo(lineIn string) (stopOut bool) {
	s.core.WriteString("The Foodogs of War have slipped!\n" + lineIn + "\n")
	return false
}

func (s *sample) DoReadline(lineIn string) (stopOut bool) {
	// Toggles between readline and basic modes
	enabled := s.core.ReadlineEnabled()
	enabled = !enabled
	err := s.core.EnableReadline(enabled)
	if err != nil {
		fmt.Println(err)
		return true
	}
	s.core.SetPrompt(prompts[enabled])
	return false
}

func (s *sample) DoWould(lineIn string) (stopOut bool) {
	s.core.WriteString(lineIn + "\n")
	return false
}

func (s *sample) CompleteWould(line string) (options []string) {
	myOptions := []string{"you", "kindly"}
	options = []string{}
	pieces := strings.Split(line, " ")
	last := pieces[len(pieces)-1]
	for _, str := range myOptions {
		if strings.HasPrefix(str, last) && (str != last) {
			options = append(options, str)
		}
	}
	if len(options) > 0 {
		return options
	} else {
		return myOptions
	}
}

func (s *sample) HelpFoo() {
	s.core.WriteString("SCHUUUULLLTZ!!!!!!\n")
}

func (s *sample) HelpAnything() {
	s.core.WriteString("This program is a demonstration of Kommandant\n")
}

func (s *sample) DefaultComplete(line string) (options []string) {
	return []string{"quux"}
}

func main() {
	demo := kommandant.NewKommandant(&sample{})
	demo.SetPrompt(prompts[false])
	// Run
	demo.CmdLoop("Kommandant demo program")
}
