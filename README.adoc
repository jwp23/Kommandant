Kommandant README
=================

== Fetching from the repository ==

go get gitlab.com/ianbruene/Kommandant

== Purpose ==

Kommandant exists to ease the creation of command line interfaces for Go
programs. It is a near port of Python's cmd module, with the caveat that
Go is a compiled language and cannot do introspection as easily. Despite this
mismatch the useful concepts are still useful after translation, and the
outward behavior will be the same.
